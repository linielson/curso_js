var grupos = ["Vestuário", "Alimentício", "Automotivo", "Calçado", "Acessário", "Decoração"];


var Produto = function(codigo, nome, grupo, modelo) {
  this.codigo = codigo;
  this.nome = nome;
  this.grupo = grupos[grupo];
  this.modelo = modelo;
  this.palavrasChave = [];
};

var carro = new Produto(7, "Gol", 2, "GTI");

console.log("\n 1. Adicionar palavras chave (Construtor) \n");
console.log(carro);
carro.palavrasChave = ["Esportivo", "Teto solar"];
console.log(carro);

var criarProduto = function(codigo, nome, grupo, modelo) {
  return {
    codigo: codigo,
    nome: nome,
    grupo: grupos[grupo],
    modelo: modelo,
    palavrasChave: []
  };
};

var abacaxi = criarProduto(3, "Abacaxi", 1, "Rei");

console.log("\n 2. Adicionar palavras chave (Factory) \n");
console.log(abacaxi);
abacaxi.palavrasChave = ["Maduro", "Doce"];
console.log(abacaxi);
