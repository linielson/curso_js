function grupos(index) {
  var arrayGrupos = ["Vestuário", "Alimentício", "Automotivo", "Calçado", "Acessário", "Decoração"];
  return arrayGrupos[index];
}

var criarProduto = function(codigo, nome, grupo, modelo) {
  var _palavrasChave = [];

  var _adicionarPalavraChave = function(palavraChave) {
    _palavrasChave.push(palavraChave);
  };

  var _removerPalavraChave = function(palavraChave) {
    _palavrasChave.splice(_palavrasChave.indexOf(palavraChave), 1);
  };

  var _getPalavrasChave = function() {
    return _palavrasChave;
  };

  return {
    codigo: codigo,
    nome: nome,
    grupo: grupos(grupo),
    modelo: modelo,
    getPalavrasChave: _getPalavrasChave,
    adicionarPalavraChave: _adicionarPalavraChave,
    removerPalavraChave: _removerPalavraChave
  };
};

var abacaxi = criarProduto(3, "Abacaxi", 1, "Rei");
console.log(abacaxi);

abacaxi.adicionarPalavraChave("Doce 1");
abacaxi.adicionarPalavraChave("Maduro");
abacaxi.adicionarPalavraChave("Doce 2");
abacaxi.adicionarPalavraChave("Doce 3");
abacaxi.removerPalavraChave("Maduro");

console.log("Original", abacaxi.getPalavrasChave());
console.log("Reverse", abacaxi.getPalavrasChave().reverse());
console.log("Original", abacaxi.getPalavrasChave());

console.log("Reverse c/ Slice", abacaxi.getPalavrasChave().slice(0).reverse());
console.log("Original", abacaxi.getPalavrasChave());
