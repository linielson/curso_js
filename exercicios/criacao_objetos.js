console.log("1. Crie uma função construtora \n");

var grupos = ["Vestuário", "Alimentício", "Automotivo", "Calçado", "Acessário", "Decoração"];

var Produto = function(codigo, nome, grupo, modelo) {
  this.codigo = codigo;
  this.nome = nome;
  this.grupo = grupos[grupo];
  this.modelo = modelo;
};

var roupa = new Produto(1, "Camiseta", 0, "Nike");
var carro = new Produto(2, "Ford Ka", 2, "Ka");

console.log(roupa);
console.log(carro);

console.log("\n 2. Crie uma função fabrica \n");

var criarProduto = function(codigo, nome, grupo, modelo) {
  return {
    codigo: codigo,
    nome: nome,
    grupo: grupos[grupo],
    modelo: modelo
  };
};

var abacaxi = criarProduto(3, "Abacaxi", 1, "Rei");
var sapato = criarProduto(4, "Sapato", 3, "Ferracine");

console.log(abacaxi);
console.log(sapato);
