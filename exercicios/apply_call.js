var grupos = ["Vestuário", "Alimentício", "Automotivo", "Calçado", "Acessário", "Decoração"];

var Produto = function(codigo, nome, grupo, modelo) {
  this.codigo = codigo;
  this.nome = nome;
  this.grupo = grupos[grupo];
  this.modelo = modelo;
};

console.log("\n 1. Usar apply \n");

var pulseira = {};
Produto.apply(pulseira, [5, "Prata", 4, "Bali"]);

console.log(pulseira);

console.log("\n 2. Usar call \n");

var vaso = {};
Produto.call(vaso, 6, "Vaso", 5, "Chines");
console.log(vaso);
