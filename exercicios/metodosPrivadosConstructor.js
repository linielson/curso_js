function grupos(index) {
  var arrayGrupos = ["Vestuário", "Alimentício", "Automotivo", "Calçado", "Acessário", "Decoração"];
  return arrayGrupos[index];
}

console.log("\n 1. Constructor \n");

var Produto = function(codigo, nome, grupo, modelo) {
  var _palavrasChave = [];

  this.adicionarPalavraChave = function(palavraChave) {
    _palavrasChave.push(palavraChave);
  };

  this.removerPalavraChave = function(palavraChave) {
    _palavrasChave.splice(_palavrasChave.indexOf(palavraChave), 1);
  };

  this.getPalavrasChave = function() {
    return _palavrasChave;
  };

  this.codigo = codigo;
  this.nome = nome;
  this.grupo = grupos(grupo);
  this.modelo = modelo;
  // this.palavrasChave = _getPalavrasChave;
  // this.adicionarPalavraChave = _adicionarPalavraChave;
  // this.removerPalavraChave = _removerPalavraChave;
};

var carro = new Produto(7, "Gol", 2, "GTI");
console.log(carro);

carro.palavrasChave = ["Esportivo", "Teto solar"];
console.log(carro);

carro.adicionarPalavraChave("Roda 20");
console.log(carro);

carro.removerPalavraChave("Esportivo");
console.log(carro);
