function funcao1(x) {
  console.log("1: ", x);
  return function funcao2(x) {
    console.log("2: ", x);
  };
}

funcao1("AAAAA");
funcao1("AAAAA")("BBBBB");


function funcao1(x) {
  console.log("1: ", x);
  return function funcao2() {
    console.log("2: ", x);
  };
}

funcao1("AAAAA");
funcao1("XXXXX")();

var x = funcao1("C");
x();

//Construtora
var Pessoa = function(nome) {
  this.nome = nome;
};

var linielson = new Pessoa("Linielson");
console.log(linielson);
console.log(linielson.nome);

//Fábrica
var criarPessoa = function(nome) {
  return { nome: nome };
};

linielson = criarPessoa("Linielson Rosa");
console.log(linielson);
console.log(linielson.nome);
