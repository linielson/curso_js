//Faça um refactoring, executando a função que imprime por meio do objeto produto, utilizando this

var produto = {
  codigo: 10,
  descricao: "sapato",
  imprimir: function() {
    console.log("Produto: " + this.codigo + " - " + this.descricao);
  }
};

produto.imprimir();



var imprimir2 = function() {
  console.log("Produto: " + this.codigo + " - " + this.descricao);
};

var produto2 = {
  codigo: 10,
  descricao: "sapato",
  imprimir: imprimir2
};

produto2.imprimir();
