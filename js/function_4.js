// Utilize a função construtora para criar um novo produto por meio de call e apply, ao invés do new

//Construtora
var Produto = function(codigo, nome) {
  this.codigo = codigo;
  this.nome = nome;
};

arroz = {};
Produto.call(arroz, 1, "arroz");
console.log("Código: " + arroz.codigo + " Nome: " + arroz.nome);

feijao = {};
Produto.apply(feijao, [2, "feijão"]);
console.log(feijao);
