// Crie uma operação chamada removerPalavraChave, para remover palavras existentes

var Produto = function(codigo, nome) {
  this.codigo = codigo;
  this.nome = nome;
  this.palavrasChave = ["42", "Marrom", "Ferracine"];
  this.addPalavraChave = function(palavraChave) {
    this.palavrasChave.push(palavraChave);
  };
  this.removerPalavraChave = function(palavraChave) {
    this.palavrasChave.splice(this.palavrasChave.indexOf(palavraChave), 1);
  };
};

sapato = new Produto(1, "Sapato");
sapato.addPalavraChave("2015");
sapato.removerPalavraChave("Marrom");

console.log(sapato);
console.log(sapato.palavrasChave);

// -----------------------------------------------------------------------------

var criarProduto = function(codigo, nome, palavrasChave) {
  var _adicionarPalavraChave = function(palavraChave) {
    this.palavrasChave.push(palavraChave);
  };

  var _removerPalavraChave = function(palavraChave) {
    this.palavrasChave.splice(this.palavrasChave.indexOf(palavraChave), 1);
  };

  return {
    codigo: codigo,
    nome: nome,
    palavrasChave: [],
    addPalavraChave: _adicionarPalavraChave,
    removerPalavraChave: _removerPalavraChave
  };
};

sapato = criarProduto(1, "Sapato");
sapato.addPalavraChave("2015");
sapato.addPalavraChave("Marrom");
sapato.addPalavraChave("Calçado");
sapato.removerPalavraChave("2015");

console.log(sapato);
console.log(sapato.palavrasChave);


// Converter a função contrutora para ter métodos privados, como o exemplo acima
// Tirar a visibilidade do palavrasChave: [], criar o _getPalavrasChave
// Na funcção contrutora o que for this ficará visivel, e o que for var não ficará
