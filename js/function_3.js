// Utilize funções fábrica e construtora para criar o produto

//Construtora
var Produto = function(codigo, nome) {
  this.codigo = codigo;
  this.nome = nome;
};

var arroz = new Produto(1, "Arroz");
console.log("Código: " + arroz.codigo + " Nome: " + arroz.nome);
console.log(new Produto(2, "Feijão"));

//Fábrica
var criarProduto = function(codigo, nome) {
  return {
    codigo: codigo,
    nome: nome
  };
};

var sapato = criarProduto(3, "Sapato");
console.log("Código: " + sapato.codigo + " Nome: " + sapato.nome);
console.log(criarProduto(4, "Sandália"));
