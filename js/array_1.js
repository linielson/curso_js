//No objeto produto, inserir uma propriedade chamada palavrasChave, que é um Array, e inicializar com 3 elementos

var criarProduto = function(codigo, nome) {
  return {
    codigo: codigo,
    nome: nome,
    palavrasChave: ["42", "Marrom", "Ferracine"]
  };
};

sapato = criarProduto(3, "Sapato");
console.log(sapato);

var criarProduto = function(codigo, nome, palavrasChave) {
  return {
    codigo: codigo,
    nome: nome,
    palavrasChave: palavrasChave
  };
};

sapato = criarProduto(4, "Sapato", ["42", "Marrom", "Ferracine"]);
console.log(sapato);
