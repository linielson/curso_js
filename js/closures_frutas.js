var frutas = ["morango", "banana", "laranja", "abacaxi", "manga"];

var saladaDeFrutas = {};

for(var i = 0; i < (frutas.length - 1); i++) {
    saladaDeFrutas[frutas[i]] = function () {
        console.log("Meu nome é " + frutas[i]);
    };
}

saladaDeFrutas.morango();
saladaDeFrutas.banana();
saladaDeFrutas.laranja();
saladaDeFrutas.abacaxi();
// No momento em que a chamada foi feita, o array já estava na última posição

// Solução 1
for(var i = 0; i < (frutas.length - 1); i++) {
  saladaDeFrutas[frutas[i]] = function (fruta) {
    return function () {
      console.log("Meu nome é " + fruta);
    };
  }(frutas[i]);
}

saladaDeFrutas.morango();
saladaDeFrutas.banana();
saladaDeFrutas.laranja();
saladaDeFrutas.abacaxi();
