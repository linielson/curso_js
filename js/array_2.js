// Crie uma operação chamada adicionarPalavraChave, para inserir novas palavras

var Produto = function(codigo, nome) {
  this.codigo = codigo;
  this.nome = nome;
  this.palavrasChave = ["42", "Marrom", "Ferracine"];
  this.addPalavraChave = function(palavraChave) {
    this.palavrasChave.push(palavraChave);
  };
};

sapato = new Produto(1, "Sapato");
sapato.addPalavraChave("2015");

console.log(sapato);
console.log(sapato.palavrasChave);

// -------------------------------------------------------------

var criarProduto = function(codigo, nome, palavrasChave) {
  var _adicionarPalavraChave = function(palavraChave) {
    this.palavrasChave.push(palavraChave);
  };

  return {
    codigo: codigo,
    nome: nome,
    palavrasChave: [],
    addPalavraChave: _adicionarPalavraChave
  };
};

sapato = criarProduto(1, "Sapato");
sapato.addPalavraChave("2015");
sapato.addPalavraChave("Calçado");

console.log(sapato);
console.log(sapato.palavrasChave);
